const timeElement = document.getElementsByClassName('time')[0];
const greeter = document.getElementsByClassName('greeter')[0];
const dateElement = document.getElementsByClassName('date')[0];
const name = 'Dartandr'

const formatTime = m => {
    if(m < 10){
        return `0${m}`;
    }
    else{
        return m;
    }
}

const updateTime = () =>{
    let now = new Date();
    let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    
    if(now.getHours() < 12){
        timeElement.innerHTML = `${formatTime(now.getHours())}:${formatTime(now.getMinutes())} AM`
    }
    else{
        timeElement.innerHTML = `${formatTime(now.getHours()-12)}:${formatTime(now.getMinutes())} PM`
    }

    if(now.getHours() < 6){
        greeter.innerHTML = `Good night, ${name}`
    }
    else if(now.getHours() < 12){
        greeter.innerHTML = `Good morning, ${name}`
    }
    else if(now.getHours() < 18){
        greeter.innerHTML = `Good afternoon, ${name}`
    }
    else if(now.getHours() < 22){
        greeter.innerHTML = `Good evening, ${name}`
    }
    else{
        greeter.innerHTML = `Good night, ${name}`
    }

    dateElement.innerHTML = `${months[now.getMonth()]}-${now.getDate()}-${now.getFullYear()}`

    setTimeout(updateTime, 1000)
}

updateTime();